package net.mertsaygi.dia.requests;

import com.google.gson.Gson;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

public class LoginRequest {

    @SerializedName("login")
    private Login login;

    public LoginRequest(String username, String password) {
        this.login = new Login(username, password);
    }

    class Login {

        @SerializedName("username")
        private String username;

        @SerializedName("password")
        private String password;

        @SerializedName("disconnect_same_user")
        private boolean disconnectSameUser = true;

        @SerializedName("lang")
        private String lang = "tr";

        Login(String username, String password) {
            this.username = username;
            this.password = password;
        }

        @Override
        public String toString() {
            return new Gson().toJson(this);
        }
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
