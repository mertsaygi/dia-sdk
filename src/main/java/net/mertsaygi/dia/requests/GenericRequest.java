package net.mertsaygi.dia.requests;

import com.google.gson.annotations.SerializedName;

public class GenericRequest {

    @SerializedName("session_id")
    private String sessionId;

    @SerializedName("firma_kodu")
    private int companyCode;

    @SerializedName("donem_kodu")
    private int periodCode;

    public GenericRequest(String sessionId, int companyCode, int periodCode) {
        this.sessionId = sessionId;
        this.companyCode = companyCode;
        this.periodCode = periodCode;
    }
}
