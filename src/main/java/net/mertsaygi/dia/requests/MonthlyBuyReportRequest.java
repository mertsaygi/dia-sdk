package net.mertsaygi.dia.requests;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class MonthlyBuyReportRequest {

    @SerializedName("rpr_raporsonuc_getir")
    private MonthlyBuyReport item;

    public MonthlyBuyReportRequest(String sessionId, int companyCode, int periodCode) {
        this.item = new MonthlyBuyReport(sessionId, companyCode, periodCode);
    }

    class MonthlyBuyReport extends GenericRequest {

        public MonthlyBuyReport(String sessionId, int companyCode, int periodCode) {
            super(sessionId, companyCode, periodCode);
        }

    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
