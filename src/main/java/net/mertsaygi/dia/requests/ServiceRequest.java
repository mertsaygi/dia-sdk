package net.mertsaygi.dia.requests;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class ServiceRequest {

    @SerializedName("")
    private Service item;

    public ServiceRequest(String sessionId, int companyCode, int periodCode) {
        this.item = new Service(sessionId, companyCode, periodCode);
    }

    class Service extends GenericRequest {

        public Service(String sessionId, int companyCode, int periodCode) {
            super(sessionId, companyCode, periodCode);
        }

    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
