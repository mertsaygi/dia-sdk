package net.mertsaygi.dia.requests;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class ChangePasswordRequest {

    @SerializedName("")
    private ChangePassword changePassword;

    public ChangePasswordRequest(String sessionId, int companyCode, int periodCode) {
        this.changePassword = new ChangePassword(sessionId, companyCode, periodCode);
    }

    class ChangePassword extends GenericRequest {

        public ChangePassword(String sessionId, int companyCode, int periodCode) {
            super(sessionId, companyCode, periodCode);
        }

    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
