package net.mertsaygi.dia.requests;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class WarrantyRequest {

    @SerializedName("")
    private Warranty item;

    public WarrantyRequest(String sessionId, int companyCode, int periodCode) {
        this.item = new Warranty(sessionId, companyCode, periodCode);
    }

    class Warranty extends GenericRequest {

        private String serialNumber;

        public Warranty(String sessionId, int companyCode, int periodCode) {
            super(sessionId, companyCode, periodCode);
        }

    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
