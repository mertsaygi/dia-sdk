package net.mertsaygi.dia.requests;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import net.mertsaygi.dia.models.AccountReceiptParams;

import java.util.Date;

public class AccountReceiptRequest {

    @SerializedName("rpr_raporsonuc_getir")
    private AccountReceipt accountReceipt;

    public AccountReceiptRequest(String sessionId, int companyCode, int periodCode) {
        this.accountReceipt = new AccountReceipt(sessionId, companyCode, periodCode);
    }

    class AccountReceipt extends GenericRequest {

        @SerializedName("report_code")
        private String reportCode = "scf1110a";

        @SerializedName("tasarim_key")
        private String tasarimKey = "1200";

        @SerializedName("param")
        private AccountReceiptParams params;

        @SerializedName("format_type")
        private String formatType = "pdf";

        public AccountReceipt(String sessionId, int companyCode, int periodCode) {
            super(sessionId, companyCode, periodCode);
            params = new AccountReceiptParams();
        }
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }


}
