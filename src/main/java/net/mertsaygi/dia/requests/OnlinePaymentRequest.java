package net.mertsaygi.dia.requests;

import com.google.gson.Gson;

public class OnlinePaymentRequest {

    class OnlinePayment extends GenericRequest {

        public OnlinePayment(String sessionId, int companyCode, int periodCode) {
            super(sessionId, companyCode, periodCode);
        }

    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
