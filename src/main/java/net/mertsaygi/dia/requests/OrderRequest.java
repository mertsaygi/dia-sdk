package net.mertsaygi.dia.requests;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class OrderRequest {

    @SerializedName("")
    private Order item;

    public OrderRequest(String sessionId, int companyCode, int periodCode) {
        this.item = new Order(sessionId, companyCode, periodCode);
    }

    class Order extends GenericRequest {

        public Order(String sessionId, int companyCode, int periodCode) {
            super(sessionId, companyCode, periodCode);
        }

    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
