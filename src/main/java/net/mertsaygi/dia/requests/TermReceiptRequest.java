package net.mertsaygi.dia.requests;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class TermReceiptRequest {

    @SerializedName("rpr_raporsonuc_getir")
    private TermReceipt item;

    public TermReceiptRequest(String sessionId, int companyCode, int periodCode) {
        this.item = new TermReceipt(sessionId, companyCode, periodCode);
    }

    class TermReceipt extends GenericRequest {

        private int period;

        private boolean didDispatchesAdded;

        private boolean didUndeliveredOrdersAdded;

        public TermReceipt(String sessionId, int companyCode, int periodCode) {
            super(sessionId, companyCode, periodCode);
        }

    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
