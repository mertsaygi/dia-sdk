package net.mertsaygi.dia.requests;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class DispatchRequest{

    @SerializedName("")
    private Dispatch item;

    public DispatchRequest(String sessionId, int companyCode, int periodCode) {
        this.item = new Dispatch(sessionId, companyCode, periodCode);
    }

    class Dispatch extends GenericRequest{

        public Dispatch(String sessionId, int companyCode, int periodCode) {
            super(sessionId, companyCode, periodCode);
        }
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
