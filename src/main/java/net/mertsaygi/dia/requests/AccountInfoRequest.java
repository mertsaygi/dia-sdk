package net.mertsaygi.dia.requests;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class AccountInfoRequest {

    @SerializedName("scf_carikart_listele")
    private AccountInfo accountInfo;

    public AccountInfoRequest(String sessionId, int companyCode, int periodCode) {
        this.accountInfo = new AccountInfo(sessionId, companyCode, periodCode);
    }

    class AccountInfo extends GenericRequest {

        @SerializedName("filters")
        private String filters = "";

        @SerializedName("sorts")
        private String sorts = "";

        @SerializedName("params")
        private String params = "";

        @SerializedName("limit")
        private int limit = 10;

        @SerializedName("offset")
        private int offset = 0;

        public AccountInfo(String sessionId, int companyCode, int periodCode) {
            super(sessionId, companyCode, periodCode);
        }
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
