package net.mertsaygi.dia.models;

import com.google.gson.annotations.SerializedName;
import net.mertsaygi.dia.models.generic.Filter;
import net.mertsaygi.dia.models.generic.Group;
import net.mertsaygi.dia.models.generic.Order;


/**
 * Created by mertsaygi on 07/10/2017.
 */
public class AccountReceiptParams {

    @SerializedName("_key")
    private String key = "178717"; // ?

    @SerializedName("tarihbaslangic")
    private String startDate = "2016-01-01";

    @SerializedName("tarihbitis")
    private String endDate = "2016-12-31";

    @SerializedName("tarihreferans")
    private String dateReferance = "2016-09-08"; // ?

    @SerializedName("vadeyontem")
    private String termPeriod = "B"; // ?

    @SerializedName("vadefarki")
    private String termDifference = "0"; // ?

    @SerializedName("__ekparametreler")
    private String[] additionalParams = {"acilisbakiyesi"}; // ?

    @SerializedName("__fisturleri")
    private String[] fistures = {}; // ?

    @SerializedName("_key_sis_sube")
    private int keySysBranch = 0; // ?

    @SerializedName("_subeler")
    private String[] branches = {}; // ?

    @SerializedName("topluekstre")
    private boolean bulkReceipt = false; // ?

    @SerializedName("tekniksformgoster")
    private boolean showTechnicalForms = false; // ?

    @SerializedName("baesitsegosterme")
    private boolean hideIfBasic = false; // ?

    @SerializedName("filtreler")
    private Filter[] filters = {new Filter()};

    @SerializedName("siralama")
    private Order[] orders = {new Order()};

    @SerializedName("gruplama")
    private Group[] groups = {new Group()};

    /*@SerializedName("")
    private int period;

    @SerializedName("")
    private boolean didDispatchesAdded;

    @SerializedName("")
    private boolean didUndeliveredOrdersAdded;

    @SerializedName("")
    private boolean showVoucherDetails;

    @SerializedName("")
    private boolean didGroupedByCurrency;*/

}
