package net.mertsaygi.dia.models.generic;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mertsaygi on 08/10/2017.
 */
public class Filter {

    @SerializedName("filtreadi")
    private String filterName = "vadetarihi";

    @SerializedName("filtreturu")
    private String filterType = "aralik";

    @SerializedName("ilkdeger")
    private String firstValue = "2016-01-01";

    @SerializedName("sondeger")
    private String lastValue = "2016-12-31";

    @SerializedName("serbest")
    private String free = "pdf";

}
