package net.mertsaygi.dia.models.generic;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mertsaygi on 08/10/2017.
 */
public class Order {

    @SerializedName("fieldname")
    private String orderName = "vadetarihi";

    @SerializedName("sorttype")
    private String orderType = "asc";

}