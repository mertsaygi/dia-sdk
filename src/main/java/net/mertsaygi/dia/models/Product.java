package net.mertsaygi.dia.models;

public class Product {

    private int id;
    private String code;
    private String name;
    private int quantity;
    private String desc;
    private float itemPrice;
    private float tax;
    private float total;

    public Product(int id, String code, String name, int quantity, String desc, float itemPrice, float tax, float total) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.quantity = quantity;
        this.desc = desc;
        this.itemPrice = itemPrice;
        this.tax = tax;
        this.total = total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public float getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(float itemPrice) {
        this.itemPrice = itemPrice;
    }

    public float getTax() {
        return tax;
    }

    public void setTax(float tax) {
        this.tax = tax;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
