package net.mertsaygi.dia.models;

import java.util.Date;

public class Basket {

    private boolean didSelected;
    private String name;
    private Date createdAt;
    private float priceSum;

    public Basket(boolean didSelected, String name, Date createdAt, float priceSum) {
        this.didSelected = didSelected;
        this.name = name;
        this.createdAt = createdAt;
        this.priceSum = priceSum;
    }

    public boolean isDidSelected() {
        return didSelected;
    }

    public void setDidSelected(boolean didSelected) {
        this.didSelected = didSelected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public float getPriceSum() {
        return priceSum;
    }

    public void setPriceSum(float priceSum) {
        this.priceSum = priceSum;
    }
}
