package net.mertsaygi.dia.interceptors;

import com.google.gson.Gson;
import com.squareup.okhttp.*;
import net.mertsaygi.dia.requests.LoginRequest;
import okio.BufferedSink;

import java.io.IOException;

/**
 * Created by mertsaygi on 06/10/2017.
 */
public class DiaRequestInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        Gson gson = new Gson();
        String responseString = gson.toJson(chain.request().body());
        System.out.println(responseString);

        return chain.proceed(request);
    }
}
