package net.mertsaygi.dia;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import com.squareup.okhttp.Dispatcher;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import net.mertsaygi.dia.interceptors.DiaRequestInterceptor;
import retrofit.Converter;
import retrofit.GsonConverterFactory;
import retrofit.JacksonConverterFactory;
import retrofit.Retrofit;

import java.lang.reflect.Field;

public class DiaApi {

    private static String BASE_URL;

    public static final int FIRMA_KODU = 34;
    public static final int DONEM_KODU = 1;

    public static void init(boolean isDevelopment){
        if(isDevelopment){
            BASE_URL = "https://diademo.ws.dia.com.tr/api/v3/sis/json/";
        }
    }

    public static void init(boolean isDevelopment,String url){
        if(isDevelopment){
            init(isDevelopment);
        }else{
            BASE_URL = url;
        }
    }

    private static Gson getGsonConverter() {
        return new GsonBuilder()
                .setLenient()
                .create();
    }

    private static Retrofit getRetrofit() throws IllegalAccessException {
        if(BASE_URL == null)
            throw new IllegalAccessException("Init first...");

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient();
        client.interceptors().add(new DiaRequestInterceptor());
        //client.interceptors().add(logging);

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .validateEagerly()
                .addConverterFactory(GsonConverterFactory.create(getGsonConverter()))
                .build();
    }

    public static DiaServices getWs() {
        try {
            return DiaApi.getRetrofit().create(DiaServices.class);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

}
