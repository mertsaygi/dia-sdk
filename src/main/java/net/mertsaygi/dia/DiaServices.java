package net.mertsaygi.dia;

import com.squareup.okhttp.ResponseBody;
import net.mertsaygi.dia.requests.*;
import net.mertsaygi.dia.responses.*;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

public interface DiaServices {

    // [OK] Login
    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("/")
    Call<LoginResponse> loginService(@Body LoginRequest contact);

    // [] Online Odeme
    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("/")
    Call<OnlinePaymentResponse> makePayment(@Body OnlinePaymentRequest contact);

    // [] Cari Hesap Ekstresi
    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("/")
    Call<AccountReceiptResponse> getAccontReceipts(@Body AccountReceiptRequest contact);

    // [] Cari Vade Ekstre Raporu
    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("/")
    Call<TermReceiptResponse> getTermReceipts(@Body TermReceiptRequest contact);

    // [] Cari Aylara Göre Alış Raporu
    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("/")
    Call<MonthlyBuyReportResponse> getMonthlyReport(@Body MonthlyBuyReportRequest contact);

    // [] Cari Hesap Bilgileri
    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("/")
    Call<AccountInfoResponse> getAccountInfo(@Body AccountInfoRequest contact);

    // [] Sepetlerim

    // [] Siparişlerim
    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("/")
    Call<OrderResponse> getOrders(@Body OrderRequest contact);

    // [] İrsaliye Listesi
    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("/")
    Call<DispatchResponse> getDispatches(@Body DispatchRequest contact);

    // [] Servis Formları Listesi
    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("/")
    Call<ServiceResponse> getServiceForms(@Body ServiceRequest contact);

    // [] Seri Garanti Kontrol
    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("/")
    Call<WarrantyResponse> checkWarrantyItems(@Body WarrantyRequest contact);

    // [] Change Password
    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("/")
    Call<ChangePasswordResponse> changePassword(@Body ChangePasswordRequest contact);

}
