package net.mertsaygi.dia.responses;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("code")
    private String code;

    @SerializedName("msg")
    private String msg;

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
