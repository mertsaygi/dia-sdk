package net.mertsaygi.dia.responses;

public class AccountInfoResponse {

    private String code;
    private String title;
    private String faxplace;
    private String taxno;
    private String personal;
    private String address;
    private String phone;
    private String gsm;
    private String fax;
    private String tckimlik;

    public String getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public String getFaxplace() {
        return faxplace;
    }

    public String getTaxno() {
        return taxno;
    }

    public String getPersonal() {
        return personal;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getGsm() {
        return gsm;
    }

    public String getFax() {
        return fax;
    }

    public String getTckimlik() {
        return tckimlik;
    }
}
