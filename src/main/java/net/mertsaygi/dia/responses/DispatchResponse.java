package net.mertsaygi.dia.responses;

import net.mertsaygi.dia.models.DispatchItem;
import net.mertsaygi.dia.models.OrderItem;

import java.util.ArrayList;
import java.util.List;

public class DispatchResponse {

    List<DispatchItem> dataList = new ArrayList<DispatchItem>();

    public List<DispatchItem> getDataList() {
        return dataList;
    }
}
