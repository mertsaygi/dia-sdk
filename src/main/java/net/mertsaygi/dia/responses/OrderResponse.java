package net.mertsaygi.dia.responses;

import net.mertsaygi.dia.models.DispatchItem;
import net.mertsaygi.dia.models.OrderItem;

import java.util.ArrayList;
import java.util.List;

public class OrderResponse {

    List<OrderItem> dataList = new ArrayList<OrderItem>();

    public List<OrderItem> getDataList() {
        return dataList;
    }

}
