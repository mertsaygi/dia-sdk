package net.mertsaygi.dia.responses;

import net.mertsaygi.dia.models.DispatchItem;
import net.mertsaygi.dia.models.OrderItem;
import net.mertsaygi.dia.models.ServiceItem;

import java.util.ArrayList;
import java.util.List;

public class ServiceResponse {

    List<ServiceItem> dataList = new ArrayList<ServiceItem>();

    public List<ServiceItem> getDataList() {
        return dataList;
    }
}
