import com.google.gson.Gson;
import com.squareup.okhttp.*;
import net.mertsaygi.dia.DiaApi;
import net.mertsaygi.dia.DiaServices;
import net.mertsaygi.dia.requests.*;
import net.mertsaygi.dia.responses.LoginResponse;
import net.mertsaygi.dia.responses.ServiceResponse;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import java.io.IOException;

public class LoginTest {

    private static OkHttpClient client;
    private static Request request;

    public static void testAccountInfo(String sessionId, ResponseListener responseListener) {
        System.out.println("**********");
        // TODO: A firmasını istediğimde sadece onun sonucunu getirmeli
        AccountInfoRequest accountInfoRequest = new AccountInfoRequest(sessionId, DiaApi.FIRMA_KODU, DiaApi.DONEM_KODU);
        System.out.println("Account Info Request: " + accountInfoRequest.toString());
        request = new Request.Builder()
                .url("https://diademo.ws.dia.com.tr/api/v3/scf/json")
                .post(RequestBody.create(MediaType.parse("Content-Type: application/json;charset=UTF-8"), accountInfoRequest.toString()))
                .build();
        callRequest("Account Info Response: ", responseListener);
    }

    public static void testAccountReceipt(String sessionId, ResponseListener responseListener) {
        System.out.println("**********");
        AccountReceiptRequest accountReceiptRequest = new AccountReceiptRequest(sessionId, DiaApi.FIRMA_KODU, DiaApi.DONEM_KODU);
        System.out.println("Account Receipt Request: " + accountReceiptRequest.toString());
        request = new Request.Builder()
                .url("https://diademo.ws.dia.com.tr/api/v3/rpr/json")
                .post(RequestBody.create(MediaType.parse("Content-Type: application/json;charset=UTF-8"), accountReceiptRequest.toString()))
                .build();
        callRequest("Account Receipt Response: ", responseListener);
    }

    public static void testTermReceipt(String sessionId, ResponseListener responseListener) {
        System.out.println("**********");
        TermReceiptRequest termReceiptRequest = new TermReceiptRequest(sessionId, DiaApi.FIRMA_KODU, DiaApi.DONEM_KODU);
        System.out.println("Term Receipt Request: " + termReceiptRequest.toString());
        request = new Request.Builder()
                .url("https://diademo.ws.dia.com.tr/api/v3/rpr/json")
                .post(RequestBody.create(MediaType.parse("Content-Type: application/json;charset=UTF-8"), termReceiptRequest.toString()))
                .build();
        callRequest("Term Receipt Response: ", responseListener);
    }

    public static void testMonthlyBuyReport(String sessionId, ResponseListener responseListener) {
        System.out.println("**********");
        MonthlyBuyReportRequest monthlyBuyReportRequest = new MonthlyBuyReportRequest(sessionId, DiaApi.FIRMA_KODU, DiaApi.DONEM_KODU);
        System.out.println("Monthly Buy Report Request: " + monthlyBuyReportRequest.toString());
        request = new Request.Builder()
                .url("https://diademo.ws.dia.com.tr/api/v3/rpr/json")
                .post(RequestBody.create(MediaType.parse("Content-Type: application/json;charset=UTF-8"), monthlyBuyReportRequest.toString()))
                .build();
        callRequest("Monthly Buy Report Response: ", responseListener);
    }

    public static void testOrders(String sessionId, ResponseListener responseListener) {
        System.out.println("**********");
        OrderRequest orderRequest = new OrderRequest(sessionId, DiaApi.FIRMA_KODU, DiaApi.DONEM_KODU);
        System.out.println("Order Request: " + orderRequest.toString());
        request = new Request.Builder()
                .url("https://diademo.ws.dia.com.tr/api/v3/scf/json")
                .post(RequestBody.create(MediaType.parse("Content-Type: application/json;charset=UTF-8"), orderRequest.toString()))
                .build();
        callRequest("Order Response: ", responseListener);
    }

    public static void testDispatches(String sessionId, ResponseListener responseListener) {
        System.out.println("**********");
        DispatchRequest dispatchRequest = new DispatchRequest(sessionId, DiaApi.FIRMA_KODU, DiaApi.DONEM_KODU);
        System.out.println("Dispatch Request: " + dispatchRequest.toString());
        request = new Request.Builder()
                .url("https://diademo.ws.dia.com.tr/api/v3/scf/json")
                .post(RequestBody.create(MediaType.parse("Content-Type: application/json;charset=UTF-8"), dispatchRequest.toString()))
                .build();
        callRequest("Dispatch Response: ", responseListener);
    }

    public static void testServiceForms(String sessionId, ResponseListener responseListener) {
        System.out.println("**********");
        ServiceRequest serviceRequest = new ServiceRequest(sessionId, DiaApi.FIRMA_KODU, DiaApi.DONEM_KODU);
        System.out.println("Service Request: " + serviceRequest.toString());
        request = new Request.Builder()
                .url("https://diademo.ws.dia.com.tr/api/v3/scf/json")
                .post(RequestBody.create(MediaType.parse("Content-Type: application/json;charset=UTF-8"), serviceRequest.toString()))
                .build();
        callRequest("Service Response: ", responseListener);
    }

    public static void testWarranties(String sessionId, ResponseListener responseListener) {
        System.out.println("**********");
        WarrantyRequest warrantyRequest = new WarrantyRequest(sessionId, DiaApi.FIRMA_KODU, DiaApi.DONEM_KODU);
        System.out.println("Warranty Request: " + warrantyRequest.toString());
        request = new Request.Builder()
                .url("https://diademo.ws.dia.com.tr/api/v3/scf/json")
                .post(RequestBody.create(MediaType.parse("Content-Type: application/json;charset=UTF-8"), warrantyRequest.toString()))
                .build();
        callRequest("Warranty Response: ", responseListener);
    }

    public static void testChangePassword(String sessionId, ResponseListener responseListener) {
        System.out.println("**********");
        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest(sessionId, DiaApi.FIRMA_KODU, DiaApi.DONEM_KODU);
        System.out.println("Change Password Request: " + changePasswordRequest.toString());
        request = new Request.Builder()
                .url("https://diademo.ws.dia.com.tr/api/v3/scf/json")
                .post(RequestBody.create(MediaType.parse("Content-Type: application/json;charset=UTF-8"), changePasswordRequest.toString()))
                .build();
        callRequest("Change Password Response: ", responseListener);
    }

    public static void main(String[] args) {
        DiaApi.init(true);

        LoginRequest loginRequest = new LoginRequest("ws", "ws");

        client = new OkHttpClient();

        request = new Request.Builder()
                .url("https://diademo.ws.dia.com.tr/api/v3/sis/json/")
                .post(RequestBody.create(MediaType.parse("Content-Type: application/json;charset=UTF-8"), loginRequest.toString()))
                .build();

        client.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(com.squareup.okhttp.Response response) throws IOException {
                String responseData = response.body().string();
                System.out.println("Login Response: " + responseData);
                LoginResponse loginResponse = new Gson().fromJson(responseData, LoginResponse.class);

                testAccountInfo(loginResponse.getMsg(), new ResponseListener() {
                    @Override
                    public void gotResponse() {
                        System.out.println("**********");
                        testAccountReceipt(loginResponse.getMsg(), new ResponseListener() {
                            @Override
                            public void gotResponse() {
                                System.out.println("**********");
                                testTermReceipt(loginResponse.getMsg(), new ResponseListener() {
                                    @Override
                                    public void gotResponse() {
                                        System.out.println("**********");
                                        testMonthlyBuyReport(loginResponse.getMsg(), new ResponseListener() {
                                            @Override
                                            public void gotResponse() {
                                                System.out.println("**********");
                                                testOrders(loginResponse.getMsg(), new ResponseListener() {
                                                    @Override
                                                    public void gotResponse() {
                                                        System.out.println("**********");
                                                        testDispatches(loginResponse.getMsg(), new ResponseListener() {
                                                            @Override
                                                            public void gotResponse() {
                                                                System.out.println("**********");
                                                                testServiceForms(loginResponse.getMsg(), new ResponseListener() {
                                                                    @Override
                                                                    public void gotResponse() {
                                                                        System.out.println("**********");
                                                                        testWarranties(loginResponse.getMsg(), new ResponseListener() {
                                                                            @Override
                                                                            public void gotResponse() {
                                                                                testChangePassword(loginResponse.getMsg(), new ResponseListener() {
                                                                                    @Override
                                                                                    public void gotResponse() {

                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });


    }

    private static void callRequest(String a, ResponseListener responseListener) {
        client.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(com.squareup.okhttp.Response response) throws IOException {
                String responseData = response.body().string();
                System.out.println(a + " " + responseData);
                responseListener.gotResponse();
            }
        });
    }

    interface ResponseListener {

        void gotResponse();

    }

}
